export const APP_NAME = 'APM? Gif Tagger'

export const EDITION_MODES = {
  random: 'random',
  review: 'review',
  search: 'search'
}
