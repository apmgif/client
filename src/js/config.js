const BASE_URL = `${process.env.SERVER_URL}/`

export const API_ENDPOINTS = {
  gif: {
    byId: `${BASE_URL}gif/id`,
    byTags: `${BASE_URL}gif`,
    random: `${BASE_URL}gif/random`
  },
  gifs: {
    all: `${BASE_URL}gifs`,
    byTags: `${BASE_URL}gifs`
  }
}

export const SLACK_AUTH_URL = `` +
  `https://slack.com/oauth/authorize` +
  `?scope=commands` +
  `&client_id=${process.env.SLACK_CLIENT_ID}` +
  `&redirect_uri=${process.env.SLACK_AUTH_REDIRECT_URI}`
