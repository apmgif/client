import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { Button } from './../parts'
import translate from './../translator'

const SaveBarWrapper = styled.div`
  z-index: 9;
  display: flex;
  justify-content: flex-end;
  position: fixed;
  right: 20px;
  top: 60px;
`

const SaveBar = ({ onSave, enabled = true, t }) => (
  <SaveBarWrapper>
    <Button
      type='save'
      onClick={onSave}
      disabled={!enabled}
    >
      {t('label.button.save')}
    </Button>
  </SaveBarWrapper>
)

SaveBar.propTypes = {
  onSave: PropTypes.func,
  enabled: PropTypes.bool
}

export default translate(SaveBar)
export { SaveBar as root }
