import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { fixedWidth } from './../mixins'
import { FlexContainer } from './../parts'
import translate from './../translator'

const SearchBoxWrapper = styled.div`
  ${fixedWidth}
  z-index: 9;
  margin: 40px 0 20px 0;
  @media (max-width: 600px) {
    justify-content: space-between;
  }
`

const SearchBoxInput = styled.input`
  width: 100%;
  padding: 5px 0;
  font-size: 16px;
  border: none;
  border-bottom: 1px solid #ccc;
  outline: none;
  &::-webkit-input-placeholder {
    font-style: italic;
  }
`

const SearchBoxIcon = styled.div`
  float: right;
  position: relative;
  left: 2px;
  top: -28px;
  padding: 0 4px;
  cursor: pointer;
`

const ENTER_KEY = 13

class SearchBox extends Component {
  constructor (props) {
    super(props)

    this.state = {
      searchText: ''
    }

    this.handleQueryChange = this.handleQueryChange.bind(this)
    this.handleKeyUp = this.handleKeyUp.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  search () {
    const { searchText } = this.state
    if (searchText.length) {
      const query = searchText.split(' ').join(',')
      this.props.onSearch(query)
    }
  }

  handleQueryChange (event) {
    this.setState({
      searchText: event.target.value
    })
  }

  handleKeyUp ({ keyCode }) {
    if (keyCode === ENTER_KEY) {
      this.search()
    }
  }

  handleClick () {
    this.search()
  }

  render () {
    const { t } = this.props
    return (
      <FlexContainer>
        <SearchBoxWrapper>
          <SearchBoxInput
            placeholder={t('label.search-box.search-by-tags')}
            onChange={this.handleQueryChange}
            onKeyUp={this.handleKeyUp}
            value={this.state.searchText}
            type='text'
          />
          <SearchBoxIcon onClick={this.handleClick}>
            🔎
          </SearchBoxIcon>
        </SearchBoxWrapper>
      </FlexContainer>
    )
  }
}

SearchBox.propTypes = {
  onSearch: PropTypes.func,
  t: PropTypes.func
}

export default translate(SearchBox)
export { SearchBox as root }
