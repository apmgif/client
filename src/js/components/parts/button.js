import styled from 'styled-components'

const buttonTypes = {
  save: 'steelblue',
  delete: 'red',
  next: 'saddlebrown'
}

const Button = styled.button`
  cursor: ${props => props.disabled ? 'default' : 'pointer'};
  background-color: ${props => props.disabled ? 'grey' : buttonTypes[props.type]};
  ${props => props.disabled ? 'pointer-events: none;' : ''}
  color: white;
  border: none;
  border-radius: 5px;
  font-family: Ubuntu, Arial, sans-serif;
  font-size: 16px;
  padding: 10px 20px;
  text-transform: uppercase;
  outline: none;
  @media (max-width: 600px) {
    font-size: 14px;
    padding: 5px 10px;
  }

  &:hover,
  &:focus,
  &:active {
    box-shadow: ${props => props.disabled ? 'none' : '0 0 10px #aaa'};
  }

  &:focus,
  &:active {
    outline: none;
  }
`

export default Button
