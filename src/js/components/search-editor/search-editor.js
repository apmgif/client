import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { fixedWidth } from './../mixins'
import {
  Button,
  ButtonBar,
  FlexContainer
} from './../parts'
import Loader from './../loader'
import NoFilesWarning from './../no-files-warning'
import SearchBox from './../search-box'
import SingleEditor from './../single-editor'
import translate from './../translator'

import {
  fetchByTags,
  saveSingleFile,
  deleteById
} from './../../server-operations'
import { notify, errorHandling } from './../../utilities'
import sendEvent from './../../analytics'

const SearchEditorWrapper = styled.div`
  height: 100%;
`

const SingleEditorWrapper = styled.div`
  ${fixedWidth}
`

class SearchEditor extends Component {
  constructor (props) {
    super(props)

    this.state = {
      files: [],
      searchText: '',
      query: '',
      loading: false
    }

    this.handleSearch = this.handleSearch.bind(this)
    this.handleDeleteFile = this.handleDeleteFile.bind(this)
    this.handleSaveFile = this.handleSaveFile.bind(this)
    this.handleTagChange = this.handleTagChange.bind(this)
  }

  fetchFiles (query) {
    this.setState({
      files: [],
      loading: true,
      query
    }, async () => {
      try {
        const data = await fetchByTags(query)
        this.setState({
          files: data,
          loading: false
        })
      } catch (err) {
        this.setState({ loading: false })
        errorHandling(err)
      }
    })
  }

  handleSearch (query) {
    this.fetchFiles(query)
    sendEvent('SearchEditor', 'SearchImage')
  }

  handleDeleteFile (file) {
    this.setState({
      loading: true
    }, async () => {
      try {
        const response = await deleteById(file._id)
        const responseText = await response.text()

        if (!response.ok) {
          throw new Error(responseText)
        }
        notify('success', this.props.t('label.succes.file-deleted'))
        sendEvent('SearchEditor', 'DeleteImage', file.name)
        this.fetchFiles(this.state.query)
      } catch (err) {
        this.setState({ loading: false })
        errorHandling(err)
      }
    })
  }

  handleSaveFile (file) {
    this.setState({
      loading: true
    }, async () => {
      try {
        const response = await saveSingleFile(file)
        const responseText = await response.text()

        if (!response.ok) {
          throw new Error(responseText)
        }
        notify('success', this.props.t('label.success.file-updated'))
        sendEvent('SearchEditor', 'UpdateImage', file.name)
        this.fetchFiles(this.state.query)
      } catch (err) {
        this.setState({ loading: false })
        errorHandling(err)
      }
    })
  }

  handleTagChange (value, fileId) {
    if (!value.length) return

    const { files } = this.state
    const index = files.findIndex(f => f._id === fileId)

    files[index].tags = value.split(' ')
    files[index].name = value.split(' ').join('_')

    this.setState({
      files: [...files]
    })
  }

  render () {
    const { t } = this.props
    const {
      files = [],
      query,
      loading
    } = this.state

    return (
      <SearchEditorWrapper>
        <SearchBox onSearch={this.handleSearch} />
        <FlexContainer flex='1'>
          {
            loading &&
            <Loader />
          }
          {
            !loading && query.length > 0 && !files.length &&
            <NoFilesWarning>{t('label.warning.no-matching-files')}</NoFilesWarning>
          }
          {
            !loading && files.length > 0 &&
            <SingleEditorWrapper>
              {
                files.map((file, index) => (
                  <SingleEditor
                    key={index}
                    file={file}
                    onTagChange={this.handleTagChange}
                  >
                    <ButtonBar>
                      <Button type='save' onClick={() => this.handleSaveFile(file)}>
                        {t('label.button.save')}
                      </Button>
                      <Button type='delete' onClick={() => this.handleDeleteFile(file)}>
                        {t('label.button.delete')}
                      </Button>
                    </ButtonBar>
                  </SingleEditor>
                ))
              }
            </SingleEditorWrapper>
          }
        </FlexContainer>
      </SearchEditorWrapper>
    )
  }
}

SearchEditor.propTypes = {
  t: PropTypes.func
}

export default translate(SearchEditor)
export { SearchEditor as root }
