import { css } from 'styled-components'

export const flexContainer = css`
  display: flex;
  justify-content: center;
`

export const fixedWidth = css`
  width: 100%;
  max-width: 900px;
  @media (max-width: 900px) {
    padding: 0 20px;
  }
`
