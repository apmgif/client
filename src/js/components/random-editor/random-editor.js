import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { fixedWidth } from './../mixins'
import {
  Button,
  ButtonBar,
  FlexContainer
} from './../parts'
import Loader from './../loader'
import NoFilesWarning from './../no-files-warning'
import SingleEditor from './../single-editor'
import translate from './../translator'

import {
  fetchRandomImage,
  saveSingleFile,
  deleteById
} from './../../server-operations'
import {
  notify,
  errorHandling,
  isMobile
} from './../../utilities'
import sendEvent from './../../analytics'

const RandomEditorWrapper = styled.div`
  ${props => props.fullHeight ? 'height: 100%;' : ''}
`

const SingleEditorWrapper = styled.div`
  ${fixedWidth}
  height: 100%;
`

class RandomEditor extends Component {
  constructor (props) {
    super(props)

    this.state = {
      file: null,
      loading: true
    }

    this.handleDeleteFile = this.handleDeleteFile.bind(this)
    this.handleSaveFile = this.handleSaveFile.bind(this)
    this.handleFetchNext = this.handleFetchNext.bind(this)
    this.handleTagChange = this.handleTagChange.bind(this)
  }

  componentWillMount () {
    this.fetchRandomFile()
  }

  componentWillReceiveProps (nextProps) {
    this.fetchRandomFile()
  }

  fetchRandomFile () {
    this.setState({
      loading: true
    }, async () => {
      try {
        const data = await fetchRandomImage(this.props.tagged)
        this.setState({
          file: data && data.length ? data[0] : null,
          loading: false
        })
      } catch (err) {
        this.setState({ loading: false })
        errorHandling(err)
      }
    })
  }

  handleDeleteFile () {
    this.setState({
      loading: true
    }, async () => {
      const { file } = this.state
      try {
        const response = await deleteById(file._id)
        const responseText = await response.text()

        if (!response.ok) {
          throw new Error(responseText)
        }
        notify('success', this.props.t('label.success.file-deleted'))
        sendEvent('RandomEditor', 'DeleteImage', file.name)
        this.fetchRandomFile()
      } catch (err) {
        this.setState({ loading: false })
        errorHandling(err)
      }
    })
  }

  handleSaveFile () {
    this.setState({
      loading: true
    }, async () => {
      const { file } = this.state
      try {
        const response = await saveSingleFile(file)
        const responseText = await response.text()

        if (!response.ok) {
          throw new Error(responseText)
        }
        notify('success', this.props.t('label.success.file-updated'))
        sendEvent('RandomEditor', 'UpdateImage', file.name)
        this.fetchRandomFile()
      } catch (err) {
        this.setState({ loading: false })
        errorHandling(err)
      }
    })
  }

  handleFetchNext () {
    this.fetchRandomFile()
  }

  handleTagChange (value) {
    if (!value) {
      return
    }

    const { file } = this.state
    const tags = value.split(' ')

    this.setState({
      file: {
        ...file,
        name: tags.join('_'),
        tags
      }
    })
  }

  render () {
    const { t } = this.props
    const { file, loading } = this.state

    return (
      <RandomEditorWrapper fullHeight={isMobile()}>
        {
          loading &&
          <Loader />
        }
        {
          !loading && !file &&
          <NoFilesWarning>{t('label.warning.no-untagged-images-left')}</NoFilesWarning>
        }
        {
          !loading && !!file &&
          <FlexContainer>
            <SingleEditorWrapper>
              <SingleEditor
                file={file}
                handleTagChange={this.handleTagChange}
                onEnter={this.handleSaveFile}
                autoFocus={!isMobile()}
              >
                <ButtonBar>
                  <Button type='save' onClick={this.handleSaveFile}>
                    {t('label.button.save')}
                  </Button>
                  <Button type='delete' onClick={this.handleDeleteFile}>
                    {t('label.button.delete')}
                  </Button>
                  <Button type='next' onClick={this.handleFetchNext}>
                    {t('label.button.next')}
                  </Button>
                </ButtonBar>
              </SingleEditor>
            </SingleEditorWrapper>
          </FlexContainer>
        }
      </RandomEditorWrapper>
    )
  }
}

RandomEditor.propTypes = {
  tagged: PropTypes.bool
}

RandomEditor.defaultProps = {
  tagged: false
}

export default translate(RandomEditor)
export { RandomEditor as root }
