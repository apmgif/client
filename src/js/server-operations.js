import { API_ENDPOINTS } from './config'

async function fetchJSON (url) {
  const response = await window.fetch(url)
  return response.json()
}

async function saveJSON (url, data) {
  const headers = new window.Headers()
  headers.append('Content-Type', 'application/json')

  const payload = JSON.stringify(data)

  return window.fetch(url, {
    method: 'POST',
    headers,
    body: payload
  })
}

async function deleteRequest (url) {
  return window.fetch(url, {
    method: 'DELETE'
  })
}

export function fetchRandomImage (tagged = false) {
  return fetchJSON(`${API_ENDPOINTS.gif.random}/${tagged ? 1 : 0}`)
}

export function fetchByTags (query) {
  return fetchJSON(`${API_ENDPOINTS.gifs.byTags}/${query}`)
}

export function fetchAllImages () {
  return fetchJSON(API_ENDPOINTS.gifs.all)
}

export function saveSingleFile (file) {
  const url = `${API_ENDPOINTS.gif.byId}/${file._id}`
  return saveJSON(url, file)
}

export function saveMultipleFiles (files) {
  const url = API_ENDPOINTS.gifs.all
  return saveJSON(url, files)
}

export function deleteById (fileId) {
  const url = `${API_ENDPOINTS.gif.byId}/${fileId}`
  return deleteRequest(url)
}
