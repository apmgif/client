import styled from 'styled-components'

import { flexContainer } from './../mixins'

const FlexContainer = styled.div`
  ${flexContainer}
  flex-direction: ${props => props.direction || 'row'};
  ${props => props.flex ? `flex: ${props.flex};` : ''};
  ${props => props.overflow ? `overflow: auto;` : ''};
`

export default FlexContainer
