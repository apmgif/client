import React from 'react'
import styled from 'styled-components'

import { FlexContainer, ScreenCenteredContainer } from './../parts'

import notFound from './not-found.gif'

const H1404 = styled.h1`
  font-size: 60px;
  margin-bottom: 20px;
`

const NotFound = () => {
  return (
    <FlexContainer>
      <ScreenCenteredContainer>
        <H1404>404</H1404>
        <img src={notFound} alt='Qué sé yo' />
      </ScreenCenteredContainer>
    </FlexContainer>
  )
}

export default NotFound
