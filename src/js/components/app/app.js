import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import Header from './../header'
import Home from './../home'
import RandomEditor from './../random-editor'
import SearchEditor from './../search-editor'
import NotFound from './../not-found'
import ToastrNotifier from './../toastr-notifier'
import { FlexContainer } from './../parts'

import { EDITION_MODES } from './../../constants'

const AppWrapper = styled.div`
  height: 100%;
`
AppWrapper.displayName = 'AppWrapper'

const ContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`
ContentWrapper.displayName = 'ContentWrapper'

class App extends Component {
  getChildContext () {
    return {
      language: this.props.language
    }
  }

  render () {
    return (
      <AppWrapper>
        <Router>
          <ContentWrapper>
            <Header routes={EDITION_MODES} />
            <FlexContainer direction='column' flex={1} overflow>
              <Switch>
                <Route exact path='/' component={Home} />
                <Route path='/random' render={() => <RandomEditor />} />
                <Route path='/review' render={() => <RandomEditor tagged />} />
                <Route path='/search' component={SearchEditor} />
                <Route component={NotFound} />
              </Switch>
            </FlexContainer>
          </ContentWrapper>
        </Router>
        <ToastrNotifier />
      </AppWrapper>
    )
  }
}

App.childContextTypes = {
  language: PropTypes.string
}

App.propTypes = {
  language: PropTypes.string
}

export default App
