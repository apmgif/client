import React from 'react'
import styled from 'styled-components'

import { ScreenCenteredContainer } from './../parts'
import SlackButton from './../slack-button'

import apm from './apm.png'

const SlackButtonWrapper = styled.div`
  margin-top: 20px;
`

const Image = styled.img`
  @media (max-width: 600px) and (orientation: portrait) {
    width: 80%;
  }
  @media (orientation: landscape) {
    height: 70%;
  }
`

const Home = () => {
  return (
    <ScreenCenteredContainer>
      <Image src={apm} alt='APM?' />
      <SlackButtonWrapper>
        <SlackButton />
      </SlackButtonWrapper>
    </ScreenCenteredContainer>
  )
}

export default Home
