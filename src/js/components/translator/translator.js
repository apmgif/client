import React, { Component } from 'react'
import PropTypes from 'prop-types'

import en from '../../../translations/en.json'
import ca from '../../../translations/ca.json'

const languages = {
  en,
  ca
}

export function translateFunction (lang) {
  const language = languages[lang]

  return (key) => language ? language[key] : key
}

export default function translate (ChildComponent) {
  class Translator extends Component {
    render () {
      const { language } = this.context
      const t = translateFunction(language)

      return (
        <ChildComponent {...this.props} t={t} />
      )
    }
  }

  Translator.contextTypes = {
    language: PropTypes.string
  }

  return Translator
}
