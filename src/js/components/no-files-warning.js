import React from 'react'
import styled from 'styled-components'

import { ScreenCenteredContainer } from './parts'

const NoFilesWarningWrapper = styled.h2`
  color: crimson;
  text-align: center;
`

const NoFilesWarning = ({ children }) => (
  <ScreenCenteredContainer>
    <NoFilesWarningWrapper>
      {children}
    </NoFilesWarningWrapper>
  </ScreenCenteredContainer>
)

export default NoFilesWarning
