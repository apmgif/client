import styled from 'styled-components'
import FlexContainer from './flex-container'

const ButtonBar = styled(FlexContainer)`
  margin: 1em 0;
  flex-direction: row;

  button {
    margin: 0 10px;
    min-width: 100px;
    opacity: .8;
    &:hover,
    &:focus,
    &:active {
      opacity: 1;
    }
    @media (max-width: 600px) {
      min-width: 80px;
    }
  }
`

export default ButtonBar
