export default (eventCategory, eventName, eventData) => {
  window.ga && window.ga('send', 'event', eventCategory, eventName, eventData)
}
