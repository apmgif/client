import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import FileTag from './../file-tag'
import { noop } from './../../utilities'

const SingleEditorWrapper = styled.div`
  padding: 10px 0;
  flex: 1;
  &:not(:last-child) {
    border-bottom: 1px solid #eee;
  }
`

const SingleEditor = (props) => {
  const {
    file,
    children,
    onTagChange,
    onEnter,
    autoFocus
  } = props

  return (
    <SingleEditorWrapper>
      <FileTag
        file={file}
        onTagChange={onTagChange}
        onEnter={onEnter}
        autoFocus={autoFocus}
      />
      {children}
    </SingleEditorWrapper>
  )
}

SingleEditor.propTypes = {
  file: PropTypes.object,
  children: PropTypes.node,
  onTagChange: PropTypes.func,
  onEnter: PropTypes.func,
  autoFocus: PropTypes.bool
}

SingleEditor.defaultProps = {
  autoFocus: false,
  onEnter: noop,
  onTagChange: noop
}

export default SingleEditor
