import React, { Component } from 'react'

import Toast from './toast'

class ToastrNotifier extends Component {
  constructor (props) {
    super(props)

    this.state = {
      type: 'info',
      message: ''
    }

    this.notification = this.notification.bind(this)
  }

  componentDidMount () {
    document.addEventListener('toastify', this.notification)
  }

  componentWillUnmount () {
    document.removeEventListener('toastify', this.notification)
  }

  notification ({ detail }) {
    const { type, message, timeout } = detail
    this.setState({
      type,
      message,
      timeout
    })
  }

  render () {
    return (
      <div>
        <Toast
          type={this.state.type}
          message={this.state.message}
          timeout={this.state.timeout}
        />
      </div>
    )
  }
}

export default ToastrNotifier
