import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const backgroundColors = {
  info: 'steelblue',
  success: '#2D936C',
  error: 'red'
}

const ToastWrapper = styled.div`
  position: fixed;
  bottom: 15px;
  right: 15px;
  min-height: 15px;
  width: 200px;
  box-shadow: 1px 1px 10px #ccc;
  border-radius: 2px;
  padding: 15px 20px;
  font-size: 16px;
  font-weight: bold;
  color: white;
  opacity: .8;
  background-color: ${props => backgroundColors[props.type]};
`

class Toast extends Component {
  constructor (props) {
    super(props)

    this.state = {
      visible: true
    }
  }

  componentWillReceiveProps (nextProps) {
    this.setTimer()
    this.setState({
      visible: true
    })
  }

  componentDidMount () {
    this.setTimer()
  }

  componentWillUnmount () {
    clearTimeout(this.timer)
  }

  setTimer () {
    // clear any existing timer
    if (this.timer !== null) {
      clearTimeout(this.timer)
    }

    // hide after `timeout` milliseconds
    this.timer = setTimeout(() => {
      this.setState({
        visible: false
      })
      this.timer = null
    }, this.props.timeout)
  }

  render () {
    if (!this.state.visible || !this.props.message) return null

    return (
      <ToastWrapper {...this.props}>
        {this.props.message}
      </ToastWrapper>
    )
  }
}

Toast.propTypes = {
  timeout: PropTypes.number,
  message: PropTypes.string,
  type: PropTypes.string
}

Toast.defaultProps = {
  timeout: 2000
}

export default Toast
