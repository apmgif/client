import React from 'react'
import styled, { keyframes } from 'styled-components'

import { flexContainer } from './mixins'
import translate from './translator'

const Spinner = styled.div`
  margin: 100px auto 0;
  width: 100px;
  text-align: center;
`

const bouncedelay = keyframes`
  0%, 80%, 100% { 
    transform: scale(0);
  } 40% { 
    transform: scale(1.0);
  }
`

const SpinnerBall = styled.div`
  width: 18px;
  height: 18px;
  background-color: #333;
  border-radius: 100%;
  display: inline-block;  
  animation: ${bouncedelay} 1.4s infinite ease-in-out both;
  margin: 0 4px;
`

const SpinnerBall1 = styled(SpinnerBall)`
  animation-delay: -0.32s;
`

const SpinnerBall2 = styled(SpinnerBall)`
  animation-delay: -0.16s;
`

const LoaderWrapper = styled.div`
${flexContainer}
  width: 100%;
  height: 100%;
  align-items: center;
`

const Loader = ({ t }) => {
  return (
    <LoaderWrapper>
      <Spinner>
        <SpinnerBall1 />
        <SpinnerBall2 />
        <SpinnerBall />
      </Spinner>
    </LoaderWrapper>
  )
}

export default translate(Loader)
export { Loader as root }
