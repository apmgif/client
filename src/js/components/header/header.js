import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import styled, { css } from 'styled-components'

import translate from './../translator'
import { APP_NAME } from './../../constants'

const HeaderWrapper = styled.header`
  z-index: 10;
  height: 25px;
  padding: 10px 20px;
  background-color: #AD372B;
  box-shadow: 0 0 5px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`

const HeaderTitle = styled.h1`
  font-family: 'BenchNine', sans-serif;
  font-weight: bold;
  font-size: 40px;
  color: white;
  text-shadow: -3px 3px 10px black;
  margin: 0 0 0 20px;
  @media (max-width: 300px) {
    font-size: 24px;
    margin: 0;
  }
  @media (min-width: 300px) and (max-width: 600px) {
    font-size: 30px;
    margin: 0;
  }
  a {
    color: white;
    text-decoration: none;
    &:visited,
    &:hover,
    &:active {
      color: white;
    }
  }
`

const ModeSelector = styled.ul`
  list-style: none;
  @media (max-width: 600px) {
    display: none;
  }
`

const modeSelectorAnchor = css`
  a {
    font-size: 16px;
    text-decoration: none;
    text-transform: uppercase;
    color: white;
    &:hover,
    &:focus,
    &:active,
    &.active {
      text-decoration: underline;
      font-weight: bold;
      color: #000;
    }
  }
`

const ModeSelectorOption = styled.li`
  display: inline;
  margin: 0 20px;
  ${modeSelectorAnchor}
`

const HeaderButton = styled.div`
  display: none;
  border: 1px solid white;
  border-radius: 4px;
  padding: 0 5px;
  @media (max-width: 600px) {
    display: inline-block;
  }
`

const HeaderButtonBar = styled.div`
  width: 30px;
  height: 3px;
  background-color: white;
  margin: 6px 0;
  border-radius: 4px;
  box-shadow: -3px 3px 10px black;
`

const ModeSelectorDropdown = styled.ul`
  width: 100px;
  background-color: #AD372B;
  box-shadow: -2px 2px 4px #777;
  list-style: none;
  padding: 0;
  z-index: 10;
  position: absolute;  
  top: 27px;
  right: 0;
`

const ModeSelectorDropdownOption = styled.li`
  padding: 10px 0;
  border-top: 1px solid #c9655c;
  text-align: center;
  ${modeSelectorAnchor}
`

class Header extends Component {
  constructor (props) {
    super(props)

    this.state = {
      showDropdown: false
    }

    this.routeKeys = Object.keys(props.routes)

    this.toggleDropdownMenu = this.toggleDropdownMenu.bind(this)
    this.hideDropdownMenu = this.hideDropdownMenu.bind(this)
  }

  toggleDropdownMenu () {
    this.setState({
      showDropdown: !this.state.showDropdown
    })
  }

  hideDropdownMenu () {
    this.setState({
      showDropdown: false
    })
  }

  render () {
    const { t, routes } = this.props
    const menuItems = OptionComponent => this.routeKeys.map(routeKey => {
      return (
        <OptionComponent
          key={routeKey}>
          <NavLink
            to={`/${routes[routeKey]}`}
            onClick={this.hideDropdownMenu}
            activeClassName='active'
          >{t(`label.menu.${routeKey}`)}
          </NavLink>
        </OptionComponent>
      )
    })

    return (
      <HeaderWrapper>
        <HeaderTitle>
          <NavLink
            to={`/`}
            onClick={this.hideDropdownMenu}
            activeClassName='active'
          >{APP_NAME}
          </NavLink>
        </HeaderTitle>
        <ModeSelector>
          {menuItems(ModeSelectorOption)}
        </ModeSelector>
        <HeaderButton onClick={this.toggleDropdownMenu}>
          <HeaderButtonBar />
          <HeaderButtonBar />
          <HeaderButtonBar />
        </HeaderButton>
        {
          this.state.showDropdown &&
          <ModeSelectorDropdown>
            {menuItems(ModeSelectorDropdownOption)}
          </ModeSelectorDropdown>
        }
      </HeaderWrapper>
    )
  }
}

Header.propTypes = {
  routes: PropTypes.object
}

Header.defaultProps = {
  routes: {}
}

export default translate(Header)
export { Header as root }
