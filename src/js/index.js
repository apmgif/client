import 'babel-polyfill'
import React from 'react'
import ReactDOM from 'react-dom'

import App from './components/app'

ReactDOM.render(<App language='ca' />, document.getElementById('root'))
