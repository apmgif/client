import React from 'react'

import { SLACK_AUTH_URL } from '../config'

const SlackButton = () => {
  return (
    <a href={SLACK_AUTH_URL}>
      <img
        alt='Add to Slack'
        height='40'
        width='139'
        src='https://platform.slack-edge.com/img/add_to_slack.png'
        srcSet='https://platform.slack-edge.com/img/add_to_slack.png 1x, https://platform.slack-edge.com/img/add_to_slack@2x.png 2x'
      />
    </a>
  )
}

export default SlackButton
