import React, { Component } from 'react'
import { PropTypes } from 'prop-types'
import styled from 'styled-components'

import translate from './../translator'
import { noop } from './../../utilities'

const ENTER_KEY = 13

const FileTagWrapper = styled.div`
  margin: 10px 0;
`

const FileTagInput = styled.input`
  border: 1px solid #ccc;
  margin-top: 10px;
  font-size: 14px;
  padding: 5px;
  &:read-only,
  &:disabled {
    background-color: #f0f0f0;
  }
`

const FileTagImageWrapper = styled.div`
  display: flex;
  justify-content: center;
`

const FileTagImage = styled.img`
  display: block;
  max-width: 100%;
`

const FileTagFields = styled.div`
  margin: 1em 0;
`

const FileTagFieldsParagraph = styled.p`
  line-height: 10px;
  display: flex;
  margin: 25px 0;
  flex-direction: column;
`

const FileTagFieldsLabel = styled.label`
  display: inline-block;
`

class FileTag extends Component {
  constructor (props) {
    super(props)

    this.state = {
      tags: props.file.tags.join(' ')
    }

    this.handleTagChange = this.handleTagChange.bind(this)
    this.handleTagBlur = this.handleTagBlur.bind(this)
    this.handleKeyUp = this.handleKeyUp.bind(this)
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      tags: nextProps.file.tags.join(' ')
    })
  }

  handleTagChange (event) {
    this.setState({
      tags: event.target.value
    })
  }

  handleTagBlur () {
    this.props.onTagChange(this.state.tags, this.props.file._id)
  }

  handleKeyUp ({ keyCode }) {
    if (keyCode === ENTER_KEY) {
      this.handleTagBlur()
      this.props.onEnter()
    }
  }

  stripFileExtension (fileName) {
    return fileName.indexOf('.') > 0
      ? fileName.substring(0, fileName.lastIndexOf('.'))
      : fileName
  }

  render () {
    const { file, autoFocus, t } = this.props

    return (
      <FileTagWrapper>
        <div>
          <FileTagImageWrapper>
            <FileTagImage src={file.path} alt={file.name} />
          </FileTagImageWrapper>
          <FileTagFields>
            {
              !!file.hits &&
              <FileTagFieldsParagraph>
                <FileTagFieldsLabel>
                  <strong>{t('label.editor.matching-tags')} {file.hits}</strong>
                </FileTagFieldsLabel>
              </FileTagFieldsParagraph>
            }
            <FileTagFieldsParagraph>
              <FileTagFieldsLabel>{t('label.editor.filename')}</FileTagFieldsLabel>&nbsp;
              <FileTagInput
                type='text'
                readOnly
                value={this.stripFileExtension(file.name)}
              />
            </FileTagFieldsParagraph>
            <FileTagFieldsParagraph>
              <FileTagFieldsLabel>{t('label.editor.tags')}</FileTagFieldsLabel>&nbsp;
              <FileTagInput
                type='text'
                onBlur={this.handleTagBlur}
                onChange={this.handleTagChange}
                onKeyUp={this.handleKeyUp}
                value={this.state.tags}
                autoFocus={autoFocus}
              />
            </FileTagFieldsParagraph>
          </FileTagFields>
        </div>
      </FileTagWrapper>
    )
  }
}

FileTag.propTypes = {
  file: PropTypes.object,
  onTagChange: PropTypes.func,
  onEnter: PropTypes.func,
  autoFocus: PropTypes.bool
}

FileTag.defaultProps = {
  onTagChange: noop,
  onEnter: noop
}

export default translate(FileTag)
export { FileTag as root }
