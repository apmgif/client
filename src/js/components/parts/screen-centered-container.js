import styled from 'styled-components'
import { flexContainer } from './../mixins'

const ScreenCenteredContainer = styled.div`
  ${flexContainer}
  flex-direction: column;
  align-items: center;
  flex: 1;
`

export default ScreenCenteredContainer
