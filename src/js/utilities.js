export const notify = (type, message, timeout) => {
  document.dispatchEvent(new window.CustomEvent('toastify', {
    detail: {
      type,
      message,
      timeout
    }
  }))
}

export const errorHandling = err => {
  notify('error', err.message || this.props.t('label.error.undefined'), 5000)
}

export const isMobile = () => {
  return /Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone|IEMobile/i.test(
    navigator.userAgent
  )
}

export const noop = () => {}
